import React, { Component } from 'react'
import { Route, Switch, Redirect, Router } from "react-router-dom"


import History from './History'
import Index from './components/pages/index'
import Home from './components/pages/home'

import Step1 from './components/pages/steps/step1'
import Step2 from './components/pages/steps/step2'
import Step3 from './components/pages/steps/step3'
import Step4 from './components/pages/steps/step4'
import TicketPayment from './components/pages/payment/ticket'
import CreditCardPayment from './components/pages/payment/credit-card'

const App = (props) => {
	return (
		<div className="App">
			<Router history={History}>
				<Switch>
					<Route path={`${process.env.PUBLIC_URL}/`} exact component={Index} />
					<Route path={`${process.env.PUBLIC_URL}/home`} exact component={Home} />
					<Route path={`${process.env.PUBLIC_URL}/passo-1`} exact component={Step1} />
					<Route path={`${process.env.PUBLIC_URL}/passo-2`} exact component={Step2} />
					<Route path={`${process.env.PUBLIC_URL}/passo-3`} exact component={Step3} />
					<Route path={`${process.env.PUBLIC_URL}/passo-4`} exact component={Step4} />
					<Route path={`${process.env.PUBLIC_URL}/pagamento/boleto`} exact component={TicketPayment} />
					<Route path={`${process.env.PUBLIC_URL}/pagamento/cartao`} exact component={CreditCardPayment} />
					<Redirect from="*" to={`${process.env.PUBLIC_URL}/`} />
				</Switch>
			</Router>
		</div>
	)
}
export default App
