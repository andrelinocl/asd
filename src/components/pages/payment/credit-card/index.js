import React, { Component } from "react"


import Header from '../../../elements/header'
import PageBottom from '../../../elements/page-bottom'

class CreditCardPayment extends Component {
    render() {
        return (
            <div id="CreditCardPayment">
                <Header text="Oi Ofertas" />
                <main className="home container">
                    <p>Parabéns!</p>

                    <p className="mt-5 w-75">
                        Seu pedido de adesão foi realizado com sucesso e sua linha já possui um plano Oi Controle.
                    </p>

                    <img src="/assets/images/icons/confirm.svg" alt="" className="home--confirm mt-4" />
                </main> 
                <PageBottom />
            </div>
        )
    }
}

export default CreditCardPayment