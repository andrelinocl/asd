import React, { Component } from "react"

import Header from '../../elements/header'

class Step3 extends Component {

    render() {
        return (
            <div id="Step3">
                <Header page="step" text="Passo 3 de 4" />

                <main class="home container">
                    <p>Você selecionou o Oi Controle Intermediário 6 GB</p>

                    <p class="mt-5">Como você quer pagar?</p>

                    <form action="javascript:void(0)" method="post" class="mt-4">
                        <div class="cards">
                            <div class="card">
                                <div class="card--content">
                                    <div class="card--badge"><span>MAIS<br />BARATO</span></div>
                                    <p class="card--title">Cartão de Crédito</p>
                                    <p class="card--description">É prático e vai debitar todo mês no seu cartão</p>
                                    <p class="card--value">R$ 39,99/mês</p>
                                </div>
                                <img src="assets/images/icons/arrow.svg" alt="" class="card--arrow" />
                            </div>
                        </div>

                        <div class="cards">
                            <div class="card">
                                <div class="card--content">
                                    <p class="card--title">Boleto</p>
                                    <p class="card--description">Pague em caixas eletrônicos, lotérias…</p>
                                    <p class="card--value">R$ 44,99/mês</p>
                                </div>
                                <img src="assets/images/icons/arrow.svg" alt="" class="card--arrow" />
                            </div>
                        </div>
                    </form>
                </main>
            </div>
        )
    }
}

export default Step3