import React, { Component } from "react"

import Header from '../../elements/header'

class Step4 extends Component {

    render() {
        return (
            <div id="Step4">
                <Header page="step" text="Passo 4 de 4" />

                <main className="home container">
                    <p>Você selecionou o Oi Controle Intermediário 6 GB</p>

                    <p className="mt-5">Informe o CPF do titular:</p>

                    <form action="index.html" method="post" className="mt-4">
                        <div className="group">
                            <input type="text"
                                className="has-value"
                                value="017.122.080-99" />
                            <label>CPF do Titular da Conta</label>
                        </div>

                        <div className="group">
                            <input type="text"
                                className="has-value"
                                value="017.122.080-99" />
                            <label>Número do Cartão</label>
                        </div>

                        <div className="group-flex">
                            <div className="group">
                                <select name="mes-expiracao" id="mes-expiracao" className="has-value">
                                    <option value=""></option>
                                    <option value="01">01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12" selected="">12</option>
                                </select>
                                <label>Mês de Espiração</label>
                            </div>
                            <div className="group">
                                <select name="ano-expiracao" id="ano-expiracao" className="has-value">
                                    <option value=""></option>
                                    <option value="2019">2019</option>
                                    <option value="2020">2020</option>
                                    <option value="2021">2021</option>
                                    <option value="2022">2022</option>
                                    <option value="2023">2023</option>
                                    <option value="2024">2024</option>
                                    <option value="2025" selected="">2025</option>
                                    <option value="2026">2026</option>
                                    <option value="2027">2027</option>
                                    <option value="2028">2028</option>
                                    <option value="2029">2029</option>
                                    <option value="2030">2030</option>
                                </select>
                                <label>Ano de Expiração</label>
                            </div>
                        </div>

                        <div className="group input--group">
                            <input type="text"
                                className="has-value"
                                value="643" />
                            <label>Cód. Cartão</label>

                            <div className="input--group__append">
                                <img src="assets/images/icons/card-mask.svg" alt="" />
                            </div>
                        </div>

                        <div className="group-flex">
                            <div className="group">
                                <select name="dia-nascimento" id="dia-nascimento" className="has-value">
                                    <option value=""></option>
                                    <option value="01">01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12" selected="">12</option>
                                </select>
                                <label>Dia</label>
                            </div>
                            <div className="group">
                                <select name="mes-nascimento" id="mes-nascimento" className="has-value">
                                    <option value=""></option>
                                    <option value="01">01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12" selected="">12</option>
                                </select>
                                <label>Mês</label>
                            </div>
                            <div className="group">
                                <select name="ano-nascimento" id="ano-nascimento" className="has-value">
                                    <option value=""></option>
                                    <option value="1980">1980</option>
                                    <option value="1981">1981</option>
                                    <option value="1982">1982</option>
                                    <option value="1983">1983</option>
                                    <option value="1984">1984</option>
                                    <option value="1985">1985</option>
                                    <option value="1986" selected="">1986</option>
                                    <option value="1987">1987</option>
                                    <option value="1988">1988</option>
                                    <option value="1989">1989</option>
                                    <option value="1990">1990</option>
                                    <option value="1991">1991</option>
                                </select>
                                <label>Ano</label>
                            </div>
                        </div>

                        <div className="group input--group">
                            <input type="text"
                                className="has-value"
                                value="22620-205" />
                            <label>CEP</label>
                        </div>


                        <label className="checkbox mt-4">
                            <input type="checkbox" id="checkbox-license" />
                            <div className="checkbox-control"></div>
                            <p>Afirmo que li e concordo com os <a href="javascript:void(0)" className="link">termos e condições</a>.</p>
                        </label>

                        <p className="text--muted mt-4 mb-5">
                            O plano é sem fidelização, assim, você pode cancelar quanto quiser - sem multas ou qualquer adicional.
                        </p>

                        <button type="submit"
                            className="button purple"
                            disabled="disabled"
                        >Concluir</button>
                    </form>
                </main>
            </div>
        )
    }
}

export default Step4