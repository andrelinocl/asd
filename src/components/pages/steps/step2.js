import React, { Component } from "react"

import Header from '../../elements/header'

class Step2 extends Component {

    handleChange(e) {
        this.setState({ value: e.target.value });
    }

    render() {
        return (
            <div id="Step2">
                <Header page="step" text="Passo 2 de 4" />

                <main class="home container">
                    <p>Você selecionou o Oi</p>

                    <p class="mt-5">Agora, insira o código de 6 dígitos que enviamos por SMS</p>

                    <form action="index.html" method="post" class="mt-5">
                        <div class="group-flex">
                            <div class="group">
                                <input type="text" value="4" />
                            </div>
                            <div class="group">
                                <input type="text" value="1" />
                            </div>
                            <div class="group">
                                <input type="text" value="0" />
                            </div>
                            <div class="group">
                                <input type="text" value="0" />
                            </div>
                            <div class="group">
                                <input type="text" value="9" />
                            </div>
                            <div class="group">
                                <input type="text" />
                            </div>
                        </div>

                        <a href="javascript:void(0)" class="link mt-3">
                            <img src="assets/images/icons/refresh.svg" alt="" />
                            <span>Não recebeu? Enviar novo código</span>
                        </a>
                    </form>
                </main>
            </div>
        )
    }
}

export default Step2