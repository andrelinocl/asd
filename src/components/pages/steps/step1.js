import React, { Component } from "react"

import Header from '../../elements/header'

class Step1 extends Component {

    handleChange(e) {
        this.setState({ value: e.target.value });
    }

    render() {
        return (
            <div id="Step1">
                <Header page="step" text="Passo 1 de 4" />
                <main className="home container">
                    <p>Você selecionou o Oi</p>

                    <p className="mt-5">Para continuar,<br />digite o seu número Oi:</p>

                    <form action="javascript:void(0)" method="post" className="mt-5">
                        <div className="group">
                            <input type="text"
                                className="has-value"
                                value="51 9 9288-3359"
                                onChange={ this.handleChange }
                            />
                            <label>DDD + (Número)</label>
                        </div>

                        <button type="submit"
                            className="button purple"
                            disabled="disabled">
                            Continuar
                </button>
                    </form>
                </main>
            </div>
        )
    }
}

export default Step1