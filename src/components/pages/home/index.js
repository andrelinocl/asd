import React, { Component } from "react"


import Header from "../../elements/header"
import Carousel from "../../elements/carousel"
import PageBottom from "../../elements/page-bottom"
import Modal from "../../elements/modal"


export default class Index extends Component {
    render() {
        return (
            <div id="Home">
                <Header page="default" text="Oi Ofertas"/>
                <main className="home container">
                    <h1>Mude para controle</h1>
                    <p>E resgate seu cupom de desconto nas empresas parceiras</p>
                    <Carousel />
                </main>
                <PageBottom />
                <Modal />
            </div>
        )
    }
}