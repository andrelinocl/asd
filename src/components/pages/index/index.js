import React, { Component } from "react"


export default class Index extends Component {
    render() {
        return (
            <div className="modal" id="modal-index">
                <header className="header">
                    <div className="container header--index">
                        <img src="assets/images/logos/oi.svg" alt="OI" className="brand" />

                        <h1>Oi Ofertas</h1>
                        <p>Para visualizar as ofertas, selecione o seu Estado e DDD</p>
                    </div>
                </header>

                <main className="index">
                    <div className="container">
                        <form action="index.html" method="post" className="mt-5">
                            <div className="group">
                                <select name="estado" id="estado" className="has-value">
                                    <option value=""></option>
                                    <option value="RS">Rio Grande do Sul</option>
                                    <option value="SC">Santa Catarina</option>
                                    <option value="PR">Paraná</option>
                                </select>
                                <label>Selecione o seu estado</label>
                            </div>

                            <div className="group">
                                <select name="ddd" id="ddd" disabled>
                                    <option value=""></option>
                                    <option value="51">51</option>
                                    <option value="53">53</option>
                                    <option value="55">55</option>
                                </select>
                                <label>Selecione o seu DDD</label>
                            </div>

                            <button type="submit" className="button purple " disabled>Continuar</button>
                        </form>
                    </div>
                </main>
            </div>
        )
    }
}