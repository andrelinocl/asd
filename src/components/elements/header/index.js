import React, { Component } from "react"


const Header = ({
    page,
    text
}) => (
        <header className="header">
            <div className="header--left">
                <img src="/assets/images/logos/oi.svg" alt="OI" className="brand" />
                <p className="header--text">{ text }</p>
            </div>
            {
                page === 'default' &&

                <div className="header--right">
                <a href={`${process.env.PUBLIC_URL}`} className="header--link">
                    <span>
                        <small>ddd</small> 021
                    </span>
                    alterar
                    <img src="/assets/images/icons/arrow.svg" alt="" />
                </a>
            </div>

            }
        </header>
    )

export default Header