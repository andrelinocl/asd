import React from "react"


const PageBottom = () => (
    <footer className="page--bottom">
        <h4>Comprando este plano,</h4>
        <p>você receberá acesso ao portal de ofertas da Oi, onde poderá resgatar diversos descontos em nossas lojas
                parceiras.</p>

        <div className="page--bottom__images start">
            <img src="/assets/images/logos/netshoes.svg" alt="" />
            <img src="/assets/images/logos/rappi.svg" alt="" />
            <img src="/assets/images/logos/burguer-king.svg" alt="" />
            <img src="/assets/images/logos/philco-club.svg" alt="" />
        </div>

        <div className="page--bottom__images end">
            <img src="/assets/images/logos/foto-registro.svg" alt="" />
            <img src="/assets/images/logos/livraria-cultura.svg" alt="" />
            <img src="/assets/images/logos/compra-certa.svg" alt="" />
            <img src="/assets/images/logos/kitchen-aid.svg" alt="" />
        </div>
    </footer>
)

export default PageBottom