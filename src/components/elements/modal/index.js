import React from "react"


const Modal = () => (
    <div id="modal" class="modal conditions">
            <header class="header">
                <div class="header--left">
                    <img src="assets/images/logos/oi.svg" alt="OI" class="brand" />
                </div>
                <a href="javascript:void(0)" class="header--close">
                    <span>Fechar</span>
                    <img src="assets/images/icons/close.svg" alt="" />
                </a>
            </header>

            <div class="modal--content">
                <h3 class="modal--content__title">
                    Condições de Compra
                </h3>

                <hr class="hr--purple y2" />

                <h4 class="modal--content__subtitle">Pagamento no cartão de crédito</h4>
                <div class="cards--credits">
                    <img src="assets/images/cards/visa.svg" alt="" />
                    <img src="assets/images/cards/mastercard.svg" alt="" />
                    <img src="assets/images/cards/elo.svg" alt="" />
                    <img src="assets/images/cards/hipercard.svg" alt="" />
                    <img src="assets/images/cards/diners.svg" alt="" />
                </div>
                <p class="modal--content__text">
                    Você pode realizar o pagamento via cartão de crédito das bandeiras Visa, MasterCard, Elo, Diners ou Hipercard. Valor fixo cobrado automaticamente todo mês na fatura do seu cartão. Preços promocionais sujeitos a alterações, inclusive as decorrentes de alteração nos impostos incidentes.
                </p>

                <hr class="hr--default w-100" />

                <h4 class="modal--content__subtitle">Pagamento via Boleto Digital</h4>

                <p class="modal--content__text">
                    Você pode realizar o pagamento via Boleto Digital. O código de barras do Boleto é enviado mensalmente por SMS através do número 50100 ou, se preferir, pode ser acessado pela central online de atendimento Minha Oi.<br />
                    <br />
                    Importante: após o vencimento, um novo boleto será emitido e ficará disponível na Minha Oi e nos canais de autoatendimento telefônico. O cliente também pode realizar o pagamento fazendo uma recarga igual ou superior ao valor do plano.<br />
                    <br />
                    Os clientes que selecionarem a opção de pagamento via Boleto Digital deverão realizar um pagamento único do Pacote de Benefícios através de recarga avulsa em até 5 dias corridos após a data de ativação, conforme plano selecionado:<br />
                    <br />
                    Intermediário: R$ 39,99<br />
                    Avançado: R$ 54,99<br />
                    <br />
                    Caso o cliente tenha saldo de recarga no valor igual ao plano Controle escolhido, o valor do Pacote de Benefícios será abatido automaticamente.<br />
                    <br />
                    Clientes que selecionarem a modalidade de pagamento via Boleto Digital receberão um Pacote de Benefícios (além dos benefícios do plano), válido por um mês, de acordo com o plano selecionado:<br />
                    <br />
                    Intermediário: 2 GB + 7 conteúdos do Portal de Notícias<br />
                    Avançado: 2 GB + 1500 SMS pra qualquer operadora + 10 conteúdos do Portal de Notícias.
                </p>

                <hr class="hr--default w-100" />

                <h4 class="modal--content__subtitle">Fidelização</h4>

                <p class="modal--content__text">
                    Não há multa por cancelamento.
                </p>

                <hr class="hr--default w-100" />

                <h4 class="modal--content__subtitle">Internet</h4>

                <p class="modal--content__text">
                    Até o limite da franquia contratada, você navega:<br />
                    4G — Até 5 mbps de download e 521 kbps de upload.<br />
                    3G — Até 1 mbps de download e 128 kbps de upload.<br />
                    2G — Até 78 kbps de download e 39 kbps de upload.<br />
                    <br />
                    As velocidades anunciadas são as máximas nominais, podendo variar devido a fatores externos.
                    Consulte a área de cobertura<br />
                    <br />
                    Ao atingir a franquia, o acesso à internet será interrompido.<br />
                    Para continuar navegando, você pode contratar pacotes extras de internet.
                </p>

                <hr class="hr--default w-100" />

                <h4 class="modal--content__subtitle">Serviços Adicionais</h4>

                <p class="modal--content__text">
                    Caso não queira ter acesso ao Oi Jornais, Oi Gastronomia, Oi Esportes, Oi Revistas, Vida Saudável, Clic News e Oi Livros by LivrOh!, mas queira manter as mesmas condições da sua oferta, ligue do seu Oi para o *3799.
                </p>


                <hr class="hr--default w-100" />

                <h4 class="modal--content__subtitle">Validade</h4>

                <p class="modal--content__text">
                    20/02/2019 a 14/04/2019
                </p>


                <hr class="hr--default w-100" />

                <h4 class="modal--content__subtitle">Informações Legais</h4>

                <p class="modal--content__text">
                    <a href="" class="modal--content__link">Tarifas excedentes</a><br />
                    <a href="" class="modal--content__link">Contratos e regras da oferta</a><br />
                    <a href="" class="modal--content__link">Lista de ofertas válidas para adesão</a><br />
                </p>
                
            </div>
        </div>

)

export default Modal