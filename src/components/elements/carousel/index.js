import React, { Component } from "react"


class Carousel extends Component {
    state = {
        active: false,
    };

    toggleClass() {
        const currentState = this.state.active;
        this.setState({ active: !currentState });
    };

    render() {
        return (
            <div id="Carousel">
                <div className="carousel">
                    <article className="carousel--item">
                        <header className="carousel--header">
                            <h3 className="carousel--header__text">
                                <small>SMART</small>
                                15GB
                            <span>Navegue muito mais</span>
                            </h3>
                            <div className="carousel--header__info">
                                <span>INCLUÍDO NO PLANO</span>
                                <div className="carousel--header__images">
                                    <img src="assets/images/logos/social-controle-icons.png" alt="" />
                                </div>
                                <span>SEM GASTAR DA SUA INTERNET</span>
                            </div>
                        </header>
                        <div className="carousel--content">
                            <div className="carousel--price">
                                99<span>,90<small>/mês</small></span>
                            </div>
                            <p className="carousel--price__text">sem fidelização, exclusivo No Cartão</p>
                            <a href="" className="button yellow">Selecione</a>
                        </div>
                        <footer className={ this.state.active ? 'carousel--footer active': 'carousel--footer' }>
                            <ul>
                                <li>
                                    <figure><img src="assets/images/icons/smartphone.svg" alt="" /></figure>
                                    <span>Ligações ilimitadas para todo o Brasil</span>
                                </li>
                                <li>
                                    <figure><img src="assets/images/icons/sms.svg" alt="" /></figure>
                                    <span>2000 SMS para qualquer operadora</span>
                                </li>
                                <li>
                                    <figure><img src="assets/images/icons/wi-fi.svg" alt="" /></figure>
                                    <span>Oi WIFI 2 milhões de pontos</span>
                                </li>
                                <li>
                                    <figure><img src="assets/images/icons/check.svg" alt="" /></figure>
                                    <span>Oi Revistas, Oi Jornais, Oi Livros, Clic News, Oi Esportes e Acesso a Descontos
                                    Incríveis</span>
                                </li>
                                <li>
                                    <figure></figure>
                                    <span>Consulte as <a href="javascript:void(0)" className="link link--underline">Condições de compra</a></span>
                                </li>
                            </ul>
                            <a href="javascript:void(0)" onClick={ this.toggleClass }>
                                <span>Recolher</span>
                                <img src="assets/images/icons/arrow.svg" alt="" />
                            </a>
                        </footer>
                    </article>

                    <article className="carousel--item">
                        <header className="carousel--header">
                            <h3 className="carousel--header__text">
                                <small>SMART</small>
                                15GB
                            <span>Navegue muito mais</span>
                            </h3>
                            <div className="carousel--header__info">
                                <span>INCLUÍDO NO PLANO</span>
                                <div className="carousel--header__images">
                                    <img src="assets/images/logos/social-controle-icons.png" alt="" />
                                </div>
                                <span>SEM GASTRA DA SUA INTERNET</span>
                            </div>
                        </header>
                        <div className="carousel--content">
                            <div className="carousel--price">
                                99<span>,90<small>/mês</small></span>
                            </div>
                            <p className="carousel--price__text">sem fidelização, exclusivo No Cartão</p>
                            <a href="" className="button yellow">Selecione</a>
                        </div>
                        <footer className="carousel--footer">
                            <ul>
                                <li>
                                    <img src="" alt="" />
                                    <span></span>
                                </li>
                            </ul>
                            <a href="javascript:void(0)">
                                <span>Mais Benefícios</span>
                                <img src="assets/images/icons/arrow.svg" alt="" />
                            </a>
                        </footer>
                    </article>
                </div>
                <div className="carousel--bullets">
                    <span className="active"></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
        )
    }
}

export default Carousel